class CreateAssessments < ActiveRecord::Migration
  def change
    create_table :assessments do |t|
      t.references :program, index: true, foreign_key: true
      t.references :period, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.boolean :completed

      t.timestamps null: false
    end
  end
end
