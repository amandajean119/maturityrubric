class CreatePrograms < ActiveRecord::Migration
  def change
    create_table :programs do |t|
      t.references :department, index: true, foreign_key: true
      t.string :description

      t.timestamps null: false
    end
  end
end
