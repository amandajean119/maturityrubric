class CreateAssessmenttypes < ActiveRecord::Migration
  def change
    create_table :assessmenttypes do |t|
      t.string :attype
      t.string :description

      t.timestamps null: false
    end
  end
end
