class CreateDepartments < ActiveRecord::Migration
  def change
    create_table :departments do |t|
      t.references :college, index: true, foreign_key: true
      t.string :description

      t.timestamps null: false
    end
  end
end
