class AddDepartmentReferenceToAssessment < ActiveRecord::Migration
  def change
    add_reference :assessments, :department, index: true
  end
end
