class CreateEntries < ActiveRecord::Migration
  def change
    create_table :entries do |t|
      t.references :assessment, index: true, foreign_key: true
      t.references :assessmenttype, index: true, foreign_key: true
      t.string :initial_comment
      t.integer :initial_rating
      t.string :final_comment
      t.integer :final_rating

      t.timestamps null: false
    end
  end
end
