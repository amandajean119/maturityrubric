class CreateUsersPrograms < ActiveRecord::Migration
  def self.up
    create_table :users_programs, :id => false do |t|
      t.integer :user_id
      t.integer :program_id
    end
  end
  def self.down
    drop_table :users_programs
  end
end
