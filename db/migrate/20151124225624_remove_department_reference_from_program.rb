class RemoveDepartmentReferenceFromProgram < ActiveRecord::Migration
  def change
    remove_reference :programs, :department, index: true
  end
end
