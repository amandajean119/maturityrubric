# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151124225805) do

  create_table "assessments", force: :cascade do |t|
    t.integer  "program_id"
    t.integer  "period_id"
    t.integer  "user_id"
    t.boolean  "completed"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "department_id"
  end

  add_index "assessments", ["department_id"], name: "index_assessments_on_department_id"
  add_index "assessments", ["period_id"], name: "index_assessments_on_period_id"
  add_index "assessments", ["program_id"], name: "index_assessments_on_program_id"
  add_index "assessments", ["user_id"], name: "index_assessments_on_user_id"

  create_table "assessmenttypes", force: :cascade do |t|
    t.string   "attype"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "colleges", force: :cascade do |t|
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "departments", force: :cascade do |t|
    t.integer  "college_id"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "departments", ["college_id"], name: "index_departments_on_college_id"

  create_table "entries", force: :cascade do |t|
    t.integer  "assessment_id"
    t.integer  "assessmenttype_id"
    t.string   "initial_comment"
    t.integer  "initial_rating"
    t.string   "final_comment"
    t.integer  "final_rating"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "entries", ["assessment_id"], name: "index_entries_on_assessment_id"
  add_index "entries", ["assessmenttype_id"], name: "index_entries_on_assessmenttype_id"

  create_table "periods", force: :cascade do |t|
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "programs", force: :cascade do |t|
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "first_name"
    t.string   "last_name"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

  create_table "users_programs", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "program_id"
  end

end
