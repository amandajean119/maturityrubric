at = Assessmenttype.new
at.attype = "Assessment Plan"
at.save!

at = Assessmenttype.new
at.attype = "Measurable Program Student Learning Outcomes (SLO)"
at.save!

at = Assessmenttype.new
at.attype = "Alignment of Program Learning Goals, Student Learning Outcomes, and UNM Learning Goals"
at.save!

at = Assessmenttype.new
at.attype = "Program Assessment Methods (Measures/Instruments)"
at.save!

at = Assessmenttype.new
at.attype = "Data Collection and Analysis"
at.save!

at = Assessmenttype.new
at.attype = "Implementation of Program Improvements Revisions"
at.save!
