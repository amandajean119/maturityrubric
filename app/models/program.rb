class Program < ActiveRecord::Base
  belongs_to :department
  has_and_belongs_to_many :users
end
