class Assessment < ActiveRecord::Base
  belongs_to :program
  belongs_to :period
  belongs_to :user
  belongs_to :department
  has_many :entries
  accepts_nested_attributes_for :entries
  after_create :create_new_entries
  # Program getter
  def program_description
    program.try(:description)
  end

  # Program setter
  def program_description=(description)
    self.program = Program.find_or_create_by(description: description) if description.present?
  end

  # Period getter
  def period_description
    period.try(:description)
  end

  # Period setter
  def period_description=(description)
    self.period = Period.find_or_create_by(description: description) if description.present?
  end

  # Department getter
  def department_description
    department.try(:description)
  end

  # Department setter
  def department_description=(description)
    self.department = Department.find_or_create_by(description: description) if description.present?
  end

  private

    def create_new_entries
      Assessmenttype.all.each do | atype |
        entry = Entry.new
        entry.assessment_id = self.id
        entry.assessmenttype_id = atype.id
        entry.save!
      end
    end

end
