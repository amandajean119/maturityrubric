json.array!(@colleges) do |college|
  json.extract! college, :id, :description
  json.url college_url(college, format: :json)
end
