json.array!(@assessmenttypes) do |assessmenttype|
  json.extract! assessmenttype, :id, :attype, :description
  json.url assessmenttype_url(assessmenttype, format: :json)
end
