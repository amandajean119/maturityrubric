json.array!(@programs) do |program|
  json.extract! program, :id, :department_id, :description
  json.url program_url(program, format: :json)
end
