json.array!(@entries) do |entry|
  json.extract! entry, :id, :assessment_id, :assessmenttype_id, :initial_comment, :initial_rating, :final_comment, :final_rating
  json.url entry_url(entry, format: :json)
end
