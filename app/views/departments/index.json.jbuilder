json.array!(@departments) do |department|
  json.extract! department, :id, :college_id, :description
  json.url department_url(department, format: :json)
end
