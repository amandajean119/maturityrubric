json.array!(@assessments) do |assessment|
  json.extract! assessment, :id, :program_id, :period_id, :user_id, :completed
  json.url assessment_url(assessment, format: :json)
end
