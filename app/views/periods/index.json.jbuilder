json.array!(@periods) do |period|
  json.extract! period, :id, :description
  json.url period_url(period, format: :json)
end
