class AssessmenttypesController < ApplicationController
  before_action :set_assessmenttype, only: [:show, :edit, :update, :destroy]

  # GET /assessmenttypes
  # GET /assessmenttypes.json
  def index
    @assessmenttypes = Assessmenttype.all
  end

  # GET /assessmenttypes/1
  # GET /assessmenttypes/1.json
  def show
  end

  # GET /assessmenttypes/new
  def new
    @assessmenttype = Assessmenttype.new
  end

  # GET /assessmenttypes/1/edit
  def edit
  end

  # POST /assessmenttypes
  # POST /assessmenttypes.json
  def create
    @assessmenttype = Assessmenttype.new(assessmenttype_params)

    respond_to do |format|
      if @assessmenttype.save
        format.html { redirect_to @assessmenttype, notice: 'Assessmenttype was successfully created.' }
        format.json { render :show, status: :created, location: @assessmenttype }
      else
        format.html { render :new }
        format.json { render json: @assessmenttype.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /assessmenttypes/1
  # PATCH/PUT /assessmenttypes/1.json
  def update
    respond_to do |format|
      if @assessmenttype.update(assessmenttype_params)
        format.html { redirect_to @assessmenttype, notice: 'Assessmenttype was successfully updated.' }
        format.json { render :show, status: :ok, location: @assessmenttype }
      else
        format.html { render :edit }
        format.json { render json: @assessmenttype.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assessmenttypes/1
  # DELETE /assessmenttypes/1.json
  def destroy
    @assessmenttype.destroy
    respond_to do |format|
      format.html { redirect_to assessmenttypes_url, notice: 'Assessmenttype was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_assessmenttype
      @assessmenttype = Assessmenttype.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def assessmenttype_params
      params.require(:assessmenttype).permit(:attype, :description)
    end
end
