class AssessmentsController < ApplicationController
  before_action :set_assessment, only: [:show, :edit, :update, :destroy]
  autocomplete :program, :description, :full => true
  autocomplete :period, :description, :full => true
  autocomplete :department, :description, :full => true
  # GET /assessments
  # GET /assessments.json
  def index
    @assessments = Assessment.where(user_id: current_user)
    if params[:search]
      @program = Program.description_like("%#{params[:search]}%").order('description')
    else
    end

    if params[:search]
      @period = Period.description_like("%#{params[:search]}%").order('description')
    else
    end

    if params[:search]
      @department = Department.description_like("%#{params[:search]}%").order('description')
    else
    end
  end

  # GET /assessments/1
  # GET /assessments/1.json
  def show
    @assessments = Assessment.find(params[:id])
  end

  # GET /assessments/new
  def new
    @assessment = current_user.assessments.build
  end

  # GET /assessments/1/edit
  def edit
    @assessment = Assessment.find params[:id]
    unless current_user == @assessment.user
      redirect_to(@assessment, notice: "Access denied.") and return
    end
  end

  # POST /assessments
  # POST /assessments.json
  def create
    @assessment = current_user.assessments.build(assessment_params)

    respond_to do |format|
      if @assessment.save
        flash[:success] = 'Assessment was successfully created.'
        format.html { redirect_to edit_assessment_path(@assessment), notice: 'Assessment was successfully created.' }
        format.json { render :show, status: :created, location: @assessment }
      else
        format.html { render :new }
        format.json { render json: @assessment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /assessments/1
  # PATCH/PUT /assessments/1.json
  def update
    respond_to do |format|
      if @assessment.update(assessment_params)
        flash[:success] = 'Assessment was successfully updated.'
        format.html { redirect_to edit_assessment_path(@assessment), notice: 'Assessment was successfully updated.' }
        format.json { render :show, status: :ok, location: @assessment }
      else
        flash[:danger] = 'There was a problem updating the Assessment.'
        format.html { render :edit }
        format.json { render json: @assessment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assessments/1
  # DELETE /assessments/1.json
  def destroy
    unless current_user == @assessment.user
      redirect_to(assessments_url, notice: "You can't destroy this assessment because you didn't create it.") and return
    else
      @assessment.destroy
      respond_to do |format|
        flash[:success] = 'Assessment was successfully destroyed.'
        format.html { redirect_to assessments_url, notice: 'Assessment was successfully destroyed.' }
        format.json { head :no_content }
        end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_assessment
    @assessment = Assessment.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def assessment_params
    params.require(:assessment).permit(:program_id, :period_id, :user_id, :completed, :department_id, :program_description, :period_description, :department_description, :entries_attributes => [:id, :initial_rating, :initial_comment, :final_rating, :final_comment])
  end
end
