require 'test_helper'

class AssessmenttypesControllerTest < ActionController::TestCase
  setup do
    @assessmenttype = assessmenttypes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:assessmenttypes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create assessmenttype" do
    assert_difference('Assessmenttype.count') do
      post :create, assessmenttype: { attype: @assessmenttype.attype, description: @assessmenttype.description }
    end

    assert_redirected_to assessmenttype_path(assigns(:assessmenttype))
  end

  test "should show assessmenttype" do
    get :show, id: @assessmenttype
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @assessmenttype
    assert_response :success
  end

  test "should update assessmenttype" do
    patch :update, id: @assessmenttype, assessmenttype: { attype: @assessmenttype.attype, description: @assessmenttype.description }
    assert_redirected_to assessmenttype_path(assigns(:assessmenttype))
  end

  test "should destroy assessmenttype" do
    assert_difference('Assessmenttype.count', -1) do
      delete :destroy, id: @assessmenttype
    end

    assert_redirected_to assessmenttypes_path
  end
end
